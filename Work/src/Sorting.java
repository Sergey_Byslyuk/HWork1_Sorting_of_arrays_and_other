public class Sorting {

    public static void main(String[] args) {

       bubbleSorting();
       selectionSorting();
       easySorting();

    }

    private static void bubbleSorting() {
        int fullArray[] =  arrayFilling();
        System.out.print("bubbleSorting: ");
        print(fullArray);
        for (int i = 0; i < fullArray.length-1; i++) {
            for (int j = 0; j < fullArray.length-1; j++) {
                if(fullArray[j]>fullArray[j+1]){
                    int k = fullArray[j+1];
                    fullArray[j+1] = fullArray[j];
                    fullArray[j] = k;
                }
            }
        }
        print(fullArray);
    }

    private static void selectionSorting() {
        int fullArray[] =  arrayFilling();
        System.out.print("\nselectionSorting: ");
        print(fullArray);
        for (int i = 0; i < fullArray.length-1; i++) {
           int x = fullArray[i];
           int c;
            for (int j = i+1; j < fullArray.length; j++) {
                int min = fullArray[j];
                if(min<x){
                    c = j;
                    fullArray[i]= fullArray[j];
                    fullArray[c] = x;
                }
                x = fullArray[i];
            }
        }
        print(fullArray);
    }

    private static void easySorting() { // With one cycle
        int fullArray[] =  arrayFilling();
        System.out.print("\neasySorting: ");
        print(fullArray);
        System.out.println();
        int x = 0;
        for (int i = 0; i < 10; i++) {
            if(i<5){
                System.out.print(" "+ fullArray[i]);
            }
            else {
                System.out.print(" "+ fullArray[fullArray.length-1-x]);
                x++;
            }
        }
    }



    private static int[] arrayFilling() {
        int fullArray[] = new int[10];
        for (int i = 0; i < 10; i++) {
            fullArray[i] = (int) (Math.random() *10);
        }
        return fullArray;
    }
    private static void print(int[] array) {
        System.out.println();
        for (int c: array) {
            System.out.print(" "+c);
        }
    }
}
